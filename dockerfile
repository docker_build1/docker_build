FROM debian:bookworm

# Install
RUN apt-get update
RUN apt-get install git -y

# Execute
CMD ["git","--version"]
